import React, {useState} from 'react';
import './App.css';
import OneCellItem from "../../components/OneCellItem/OneCellItem";
import Counter from "../../components/Counter/Counter";

const App = () => {
    const [oneCell, setOneCell] = useState([
        {hasItem: true, className: 'Cell', id:"c1"},
        {hasItem: true, className: 'Cell', id:"c2"},
        {hasItem: true, className: 'Cell', id:"c3"},
        {hasItem: true, className: 'Cell', id:"c4"},
        {hasItem: true, className: 'Cell', id:"c5"},
        {hasItem: true, className: 'Cell', id:"c6"},
        {hasItem: true, className: 'Cell', id:"c7"},
        {hasItem: true, className: 'Cell', id:"c8"},
        {hasItem: true, className: 'Cell', id:"c9"},
        {hasItem: true, className: 'Cell', id:"c10"},
        {hasItem: true, className: 'Cell', id:"c11"},
        {hasItem: true, className: 'Cell', id:"c12"},
        {hasItem: true, className: 'Cell', id:"c13"},
        {hasItem: true, className: 'Cell', id:"c14"},
        {hasItem: true, className: 'Cell', id:"c15"},
        {hasItem: true, className: 'Cell', id:"c16"},
        {hasItem: true, className: 'Cell', id:"c17"},
        {hasItem: true, className: 'Cell', id:"c18"},
        {hasItem: true, className: 'Cell', id:"c19"},
        {hasItem: true, className: 'Cell', id:"c20"},
        {hasItem: true, className: 'Cell', id:"c21"},
        {hasItem: true, className: 'Cell', id:"c22"},
        {hasItem: true, className: 'Cell', id:"c23"},
        {hasItem: true, className: 'Cell', id:"c24"},
        {hasItem: true, className: 'Cell', id:"c25"},
        {hasItem: true, className: 'Cell', id:"c26"},
        {hasItem: true, className: 'Cell', id:"c27"},
        {hasItem: true, className: 'Cell', id:"c28"},
        {hasItem: true, className: 'Cell', id:"c29"},
        {hasItem: true, className: 'Cell', id:"c30"},
        {hasItem: true, className: 'Cell', id:"c31"},
        {hasItem: true, className: 'Cell', id:"c32"},
        {hasItem: true, className: 'Cell', id:"c33"},
        {hasItem: true, className: 'Cell', id:"c34"},
        {hasItem: true, className: 'Cell', id:"c35"},
        {hasItem: false, className: 'Cell', id:"c36"}
    ]);

    const [cellCount, setCellCount] = useState(0);

    const [sweeperCell, setSweeperCell] = useState("O");

    const changeCell = id => {
        const index = oneCell.findIndex(cell => cell.id === id);
        const oneCellCopy = [...oneCell];
        const oneCellItemCopy = {...oneCellCopy[index]};
        oneCellItemCopy.className = 'Cell-click';
        oneCellCopy[index] = oneCellItemCopy;

        setOneCell(oneCellCopy);
        setCellCount(cellCount+1);
    }

    const cellItem = (
      <>
          <OneCellItem
            oneCell={oneCell}
            changeCell={changeCell}
          />
      </>
    );

    return (
        <div className="App">
            <div className="Cell-block">
                {cellItem}
            </div>

            <Counter
                cellCount={cellCount}
            />

            <button>Reset</button>
        </div>
    );
};

export default App;
