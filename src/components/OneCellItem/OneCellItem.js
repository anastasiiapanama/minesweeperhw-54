import React from 'react';
import Cell from "../Cell/Cell";

const OneCellItem = props => {
    return props.oneCell.map(cellItem => {
        return <Cell
            key={cellItem.id}
            className={cellItem.className}
            cellItem={cellItem.hasItem}
            changeClass={() => props.changeCell(cellItem.id)}
        />
    });
};

export default OneCellItem;