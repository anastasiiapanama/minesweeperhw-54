import React from 'react';

const Counter = props => {
    return (
        <p>Tries: {props.cellCount}</p>
    )
};

export default Counter;