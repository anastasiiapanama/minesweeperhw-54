import React from 'react';
import "./Cell.css";

const Cell = props => {
    return (
        <div className={props.className} onClick={props.changeClass}>
            {props.hasItem}
        </div>
    );
};

export default Cell;